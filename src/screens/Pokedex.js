import React, {useEffect, useReducer, useRef} from 'react';
import {
  Text,
  View,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  Button,
} from 'react-native';
import PokemonCard from '../components/PokemonCard';
import {getPokemons} from '../api/pokemon';
import {
  pokemonReducer,
  initialPokemonState,
  pokemonActionCreator,
} from '../reducers/pokemon';

export default function Pokedex() {
  const [state, dispatch] = useReducer(pokemonReducer, initialPokemonState);
  const {loading, error, pokemons, page} = state;
  const flatListRef = useRef();

  const initPokemon = {
    id: 132,
    name: 'ditto',
    sprite:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/132.png',
    types: ['normal', 'fire'],
  };

  const generatePokemon = (number, page) => {
    let pokemons = [];
    for (let index = 0; index < number; index++) {
      pokemons.push({...initPokemon, id: index + (page - 1) * number});
    }
    return pokemons;
  };

  const fetchPokemons = async () => {
    dispatch(pokemonActionCreator.loading());

    try {
      const nextPokemon = generatePokemon(100, page);
      dispatch(pokemonActionCreator.success(nextPokemon, page));
    } catch (e) {
      dispatch(pokemonActionCreator.failure());
    }
  };

  const scrollToBottom = () => {
    flatListRef.current.scrollToEnd();
  };

  useEffect(() => {
    fetchPokemons();
  }, []);

  if (pokemons.lenght === 0) {
    if (loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator animating={true} />
        </View>
      );
    } else if (error) {
      return <Text>Something Error!</Text>;
    }
  }

  return (
    <View style={{flex: 1}}>
      <Button title="ScrollToBottom" onPress={scrollToBottom} />
      <FlatList
        data={pokemons}
        ref={flatListRef}
        keyExtractor={item => item.id}
        onEndReached={() => fetchPokemons()}
        renderItem={({item}) => <PokemonCard pokemon={item} />}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
