import React, {useCallback, useState} from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Button,
  TextInput,
  Text,
} from 'react-native';

const PerformanceTest = ({navigation}) => {
  const [createNum, setCreateNum] = useState('0');
  const [instance, setInstance] = useState([]);
  const [createTimes, setCreateTimes] = useState([]);

  const onCreateNumChanged = useCallback(text => {
    setCreateNum(text.replace(/[^0-9]/g, ''));
  }, []);

  const createInstance = () => {
    const start = new Date().getTime();
    for (let index = 0; index < createNum; index++) {
      setInstance(Math.floor(index / 100));
    }
    const end = new Date().getTime();
    setCreateTimes(pre => [...pre, end - start]);
    console.log(`create time ${end - start} milliseconds`);
  };

  const getAverageCreateTime = useCallback(() => {
    if (createTimes.length === 0) {
      return 0;
    }
    return Math.floor(
      createTimes.reduce((partialSum, a) => partialSum + a, 0) /
        createTimes.length,
    );
  }, [createTimes]);

  return (
    <SafeAreaView>
      <StatusBar />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.container}>
          <TextInput
            style={styles.textInput}
            placeholder="Add instance number"
            keyboardType="numeric"
            onChangeText={onCreateNumChanged}
            value={createNum}
          />
          <Button title={`Loop ${createNum} times`} onPress={createInstance} />
          <Text
            style={
              styles.text
            }>{`Create times: ${getAverageCreateTime()} ms`}</Text>
          <Button
            title={'Gallery Test'}
            onPress={() => navigation.push('Pokedex')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInput: {
    width: '90%',
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  text: {
    marginTop: 20,
    fontSize: 18,
  },
});

export default PerformanceTest;
