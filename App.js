/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import PerformanceTest from './src/screens/PerformanceTest';
import Pokedex from './src/screens/Pokedex';

const App = () => {
  const Root = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <Root.Navigator>
        <Root.Screen name={'Performance test'} component={PerformanceTest} />
        <Root.Screen name={'Pokedex'} component={Pokedex} />
      </Root.Navigator>
    </NavigationContainer>
  );
};

export default App;
