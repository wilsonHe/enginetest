# Javascript engine on react native compared

* Init Memory is the memory usage when finished loading program.

|                          | 0.64 + JSC     | 0.64 + Hermes     | 0.64 + V8 |
|:------------------------ |:-------------- |:----------------- | --------- |
| Build Time               | 105.8s         | 119.1s            |           |
| App size                 | 9.5 MB         | 13.1 MB           |           |
| Init Memory              | 208 MB         | 189MB             |           |
| **Loop Analysis**        |                |                   |           |
| 100 times                | 3ms (10 MB)    | 8ms (2 MB)        |           |
| 10000 times              | 21ms (30 MB)   | 264ms (12 MB)     |           |
| 1000000 times            | 405ms (335 MB) | 22454ms (~200 MB) |           |
| **Flatlist Render test** | 12s (228MB)    | 37.8s (86 MB)     |           |


|                          | 0.69 + JSC     | 0.69 + Hermes     | 0.69 + V8 |
|:------------------------ |:-------------- |:----------------- | --------- |
| Build Time               | 114s           | 131s              |           |
| App size                 | 13MB           | 22.5MB            |           |
| Init Memory              | 199MB          | 192MB             |           |
| **Loop Analysis**        |                |                   |           |
| 100 times                | 4ms(15 MB)     | 7ms (1MB)         |           |
| 10000 times              | 22ms (30 MB)   | 245ms (5 MB)      |           |
| 1000000 times            | 434ms (379 MB) | 20620ms (~100 MB) |           |
| **Flatlist Render test** | 12s (195 MB)   | 34.1s (90 MB)     |           |